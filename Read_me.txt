In this script we look at taking the reported temperature in a city using the OpenWeathermap API and adding the information in a data frame while creating a few new columns to reference to the information later in a excel spreadsheet. 

1. You will need a API key you can get this from  https://openweathermap.org/
2. Paste the API key into line 7.
3. Make sure the excel file and your python file are in the same folder. 